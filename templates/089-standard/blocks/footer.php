<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2019 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer" role="contentinfo">
	<div class="footer-wrap">				
		<?php if ($this->countModules('footnav')) : ?>
			<div class="row-fluid">								
				<div class="span8 footnav">
					<div class="module_footer position_footnav">
						<jdoc:include type="modules" name="footnav" style="custom" />
					</div>			
				</div>
			</div>		
		<?php endif ?>
		<?php if($this->countModules('footer')) : ?>
			<div class="row-fluid">				
				<div class="span12 footer--module">
					<jdoc:include type="modules" name="footer" style="custom" />
				</div>
			</div>
		<?php endif;?>
		<div class="footer--bottom">		
			<div class="logo--small__footer">
				<a class="brand" href="<?php echo $this->baseurl; ?>">
					<?php echo $logo; ?>
					<?php if ($this->params->get('sitedescription')) : ?>
						<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
					<?php endif; ?>
				</a>
			</div>
		    <div class="vcard"> 
		        <p class="org">Spedition Schreyer</p>
		        <p class="adr"><span class="street-address">Schweizer Weg 1, </span>
		          <span class="postal-code">83543 </span><span class="region">Rott am Inn</span>
		         </p>
		        <p class="tel "><a class="" href="tel:+49-803940390">Tel. 08039 / 4039 - 0</a></p>
		         <p>
		         	 <a class="email" href="mailto:info@spedition-schreyer.de">info(at)spedition-schreyer.de</a>
		         </p>
		    </div>	
	   	</div>	
	</div>
	<div id="copyright" class="fullwidth">
		<div class="copyWrapper">
			<p>&copy; <?php print date("Y") . " " . $sitename; ?> | <a class="imprLink" href="/impressum.html" title="Impressum FIRMA">Impressum</a> | <a class="imprLink" href="/datenschutz.html">Datenschutz</a></p>
		</div>
	</div>	
</footer>
		