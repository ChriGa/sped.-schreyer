<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die; 

?>
<nav class="navbar-wrapper flex fullwidth">
    <div class="logo--small">
      <a class="brand" href="<?php echo $this->baseurl; ?>">
      <?php echo $logo; ?>
        <?php if ($this->params->get('sitedescription')) : ?>
          <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
        <?php endif; ?>
      </a>
    </div>
    <div class="vcard"> 
        <p class="org">Spedition Schreyer</p>
        <p class="tel "><a class="" href="tel:+49-803940390">Tel: +49 - (0) 8039 - 4039-0</a></p>
        <p class="adr"><span class="street-address">Schweizer Weg 1</span><br />
          <span class="postal-code">83543 </span><span class="region">Rott am Inn</span>
         </p>
    </div>
    <div class="navbar">
      <div class="navbar--inner">
        <?php print ($detectAgent =="phone ") ? '<button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">' : '<button type="button" class="btn btn-navbar">'; ?>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>                                
      <?php if ($this->countModules('menu')) : ?>
        <div class="nav-collapse collapse "  role="navigation">
          <jdoc:include type="modules" name="menu" style="custom" />
        </div>
      <?php endif; ?>
      </div>
    </div>
</nav>