<?php
/**
 * @author   	Copyright (C) 2019 cg@089webdesign.de
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS ?>
	<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/quicksand-regular-webfont.ttf">			
	<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/heebo-regular-webfont.ttf">
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/normalize.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/responsive.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/styles.css"; ?>" rel="stylesheet" type="text/css" />		
</head>
<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">

	<!-- Body -->
		<div id="wrapper" class="fullwidth site_wrapper">
			<div class="svg-logo">
				<?php print file_get_contents(JPATH_ROOT . '/images/spedition-schreyer-logo-grafik.svg'); ?>				
			</div>	
			<div class="above-the-fold">
			<?php									
				// including menu
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				if($this->countModules('header')) : 
					// including header
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
				endif;
				if ($isPhone && !$frontpage) : ?>
					<div class="phone-header"></div>
				<?php endif; ?>
				<?

				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');

				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			?>
			</div>
			<div class="below-the-fold">
			<?php 
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');				
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
				
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
			?>
			</div>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
	<script type="text/javascript" src="<?php print '/templates/' . $this->template . '/js/jquery.lazy.min.js';?>"></script>

	<script type="text/javascript">

		jQuery(document).ready(function() {
			<?php //lazy:?>
			<?php $thresholdValue = 0;
				(!$clientMobile) ? $thresholdValue = "30" : $thresholdValue = "0";
			?>			
			jQuery('.lazy').Lazy({
			    scrollDirection: 'vertical',
			    defaultImage: '/images/spedition-schreyer-placeholder.jpg',
			    effect: "fadeIn",
      			effectTime: 500,
			    threshold: <?php print $thresholdValue; ?>,
			    visibleOnly: true,
			    onError: function(element) {
			        console.log('error loading ' + element.data('src'));
			    }
			});			
			<?php //<--- END lazy:?>

			<?php if($clientMobile) : ?>
				<?php //mobile menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse').toggleClass('openMenu');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
				});
				<?php //if($frontpage) : ?>
					jQuery(window).scroll(function(){
						var scrollPercent = (250 - window.scrollY) / 250;
						<?php if($isTablet) : ?>
							jQuery(".item-page.start").css({ transform: "translateY(" + (jQuery(window).scrollTop() / 4) + "px)", opacity: scrollPercent });
						<?php elseif($isPhone) : ?>
							jQuery(".item-page.start, body.not_home:not(.mobileDefault) .page-header h1").css({ transform: "translateY(" + (jQuery(window).scrollTop() / 4) + "px)", opacity: scrollPercent });
						<?php endif;?>
					});
				<?php //endif;?>
			<?php else: //sticky: ?>
				jQuery(window).scroll(function(){
					if (jQuery(this).scrollTop() > 90) {
						jQuery('.navbar-wrapper').addClass('sticky'); 
						jQuery('.scroll-space').css('margin-top', '105px');
					} else {
						jQuery('.navbar-wrapper').removeClass('sticky');
						jQuery('.scroll-space').css('margin-top', '0px');	
					} 

			    //parallax scroll:
			    <?php ($frontpage) ? $logoTopValue = 15 : $logoTopValue = 45; ?>
				    var scrollPercent = (500 - window.scrollY) / 500;
				    var scrollScale = (window.scrollY / 250) + 1;
				    jQuery(".svg-logo").css({ top: <?php print $logoTopValue ?> + (jQuery(window).scrollTop() / 9 ) + "vh", opacity: scrollPercent, transform: "scale(" + scrollScale + ")"});
				    jQuery(".item-page.start").css({ transform: "translateY(" + (jQuery(window).scrollTop() / 1.12) + "px)", opacity: scrollPercent });
				    ((jQuery(window).scrollTop() / 9 ) > 120) ? jQuery('.svg-logo').css('display', 'none') : jQuery('.svg-logo').css('display', 'block');

				});

			<?php endif; ?>
			<?php // scrollToTop: ?>
				jQuery(function(){	 
					jQuery(document).on( 'scroll', function(){
				 
						if (jQuery(window).scrollTop() > 100) {
							jQuery('.scroll-top-wrapper').addClass('show');
						} else {
							jQuery('.scroll-top-wrapper').removeClass('show');
						}
					});	 
					jQuery('.scroll-top-wrapper').on('click', scrollToTop);
				});	 
				function scrollToTop() {
					verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
					element = jQuery('body');
					offset = element.offset();
					offsetTop = offset.top;
					jQuery('html, body').animate({scrollTop: offsetTop}, 500, 'swing');
				}			
	});

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<span aria-hidden="true">&#8679</span>
	</span>
</div>	
</body>
</html>
